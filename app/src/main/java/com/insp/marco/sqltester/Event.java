package com.insp.marco.sqltester;

/**
 * Created by Marco on 12/10/2015.
 */
public class Event {
    private int _id;
    private String name;
    private String start_time;
    private Venue venue;

    public Event() {}

    public Event(String name, String start_time, String[] venue) {
        this.name = name;
        this.start_time = start_time;
        this.venue = new Venue(venue);
    }

    public String getName() {
        return name;
    }

    public String getStart_time() {
        return start_time;
    }

    public String getVenue() {
        return venue.toString();
    }

    @Override
    public String toString() {
        return "Event{" +
                "name='" + name + '\'' +
                ", start_time='" + start_time + '\'' +
                ", venue=" + venue +
                '}';
    }
}
