package com.insp.marco.sqltester;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import android.widget.Toast;
import java.io.*;


public class MainActivity extends AppCompatActivity {

    EditText txtInput;
    TextView txtShow;
    MyDBHandler dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtInput = (EditText) findViewById(R.id.txtInput);
        txtShow = (TextView) findViewById(R.id.txtShow);
        dbHandler = new MyDBHandler(this, null, null, 1);

        printDatabase();
    }

    // Add event to database
    public void addObject(View view) {
        Event event = new Event(txtInput.getText().toString(), "today", new String[] {"lat", "long"});
        dbHandler.addEvent(event);
        printDatabase();
    }

    // Delete events
    public void delObject(View view) {
        String inputText = txtInput.getText().toString();
        dbHandler.deleteEvent(inputText);
        printDatabase();
    }

    public void printDatabase() {
        String dbString = dbHandler.toString();
        txtShow.setText(dbString);
        txtInput.setText("");
    }
}
