package com.insp.marco.sqltester;

/**
 * Created by Marco on 12/10/2015.
 */
public class Venue {
    private String latitude;
    private String longitude;
    private String latlong_confidence;
    private String street;
    private String zip;
    private String city;
    private String state;
    private String country;
    private String id;


    public Venue() {}

    public Venue(String[] venue) {
        this.latitude = venue[0];
        this.longitude = venue[1];
        this.latlong_confidence = latlong_confidence;
        this.street = street;
        this.zip = zip;
        this.city = city;
        this.state = state;
        this.country = country;
        this.id = id;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    @Override
    public String toString() {
        return "Location: " + latitude + "." + longitude;
    }
}
